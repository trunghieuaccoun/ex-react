import logo from './logo.svg';
import './App.css';
import Ex_Glass from './Ex_Glass/Ex_Glass';
import Ex_ShoeShop from './Ex_ShoeShop/Ex_ShoeShop';
import Ex_ShoeShop_Redux from './Ex_ShoeShop_Redux/Ex_ShoeShop_Redux';
import Ex_TaiXiu from './Ex_TaiXiu/Ex_TaiXiu';

function App() {
    return (
        <div className='App'>
            {/* <Ex_Glass /> */}
            {/* <Ex_ShoeShop /> */}
            {/* <Ex_ShoeShop_Redux /> */}
            <Ex_TaiXiu />
        </div>
    );
}

export default App;
