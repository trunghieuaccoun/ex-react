import React, { Component } from 'react';

export default class ItemShoe extends Component {
    render() {
        let { image, name, price } = this.props.dataRow;

        return (
            <div className='col-3 p-1'>
                <div className='card text-white bg-primary '>
                    <img className='card-img-top' src={image} alt='' />
                    <div className='card-body'>
                        <h4 className='card-title'>{name}</h4>
                        <p className='card-text'>{price}</p>
                    </div>
                </div>
            </div>
        );
    }
}
