import React, { Component } from 'react';
import { connect } from 'react-redux';
import ItemShoe from './ItemShoe';

export class ListShoe extends Component {
    renderListShoe = () => {
        return this.props.listShoe.map((item, index) => {
            console.log(item);
            return <ItemShoe dataRow={item} key={index} />;
        });
    };
    render() {
        return <div className='row'>{this.renderListShoe}</div>;
    }
}

const mapStateToProps = (state) => {
    return { listShoe: state.shoeReducer.shoeArr };
};

export default connect(mapStateToProps)(ListShoe);
// rcredux
