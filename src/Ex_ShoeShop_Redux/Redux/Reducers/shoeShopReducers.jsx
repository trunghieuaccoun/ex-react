import { dataShoe } from './../../dataShoe';

let initialState = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
};

export const shoeReducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};
// rxredux
