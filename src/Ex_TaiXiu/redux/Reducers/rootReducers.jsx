import { combineReducers } from 'redux';
import { xucSacReducers } from './reducers_TaiXiu';

export const rootReducers_TaiXiu = combineReducers({ xucSacReducers });
