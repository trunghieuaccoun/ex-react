let initialState = {
    arrayXucSac: [
        {
            img: './imgXucSac/1.png',
            giaTri: 1,
        },
        {
            img: './imgXucSac/1.png',
            giaTri: 1,
        },
        {
            img: './imgXucSac/1.png',
            giaTri: 1,
        },
    ],
    resutl: 3,
    youChoose: 0,
    youWin: 0,
    total: 0,
};

export const xucSacReducers = (state = initialState, action) => {
    switch (action.type) {
        case 'PLAY_GAME': {
            let showresult = 0;
            let newArray = state.arrayXucSac.map((item) => {
                let random = Math.floor(Math.random() * 6 + 1);
                showresult = random + showresult;
                return {
                    img: `./imgXucSac/${random}.png`,
                    giaTri: random,
                    showresult,
                };
            });
            let totalClone = state.total + 1;
            return {
                ...state,
                arrayXucSac: newArray,
                total: totalClone,
                resutl: showresult,
            };
        }
        // case 'YOU_CHOOSE': {
        //     let youChooseClone = value;
        //     return { ...state, youChoose: youChooseClone };
        // }
        default:
            return state;
    }
};
