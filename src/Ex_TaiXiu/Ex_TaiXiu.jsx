import React, { Component } from 'react';
import KetQua from './KetQua';
import XucSac from './XucSac';
import imgBg from '../assets/bgGame.png';
export default class Ex_TaiXiu extends Component {
    render() {
        return (
            <div
                style={{
                    background: `url(${imgBg})`,
                    width: '100vw',
                    height: '100vh',
                }}
                className='py-5'
            >
                <XucSac />
                <KetQua />
            </div>
        );
    }
}
