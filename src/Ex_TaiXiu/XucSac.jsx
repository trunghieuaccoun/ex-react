import React, { Component } from 'react';
import { connect } from 'react-redux';

export class XucSac extends Component {
    state = {
        active: null,
    };
    handleChangeActive = (value) => {
        this.setState({ active: value });
    };
    renderListXucXac = () => {
        return this.props.arrayXucSac.map((item, index) => {
            return (
                <img
                    src={item.img}
                    key={index}
                    style={{
                        width: 100,
                        margin: 10,
                        borderRadius: 8,
                    }}
                    alt='lỗi nè'
                />
            );
        });
    };
    renderResutl = () => {
        let { resutl, youChoose, youWin, total } = this.props;
        return (
            <div>
                <p>youChoose : {youChoose}</p>
                <p>youwin : {youWin}</p>
                <p>total : {total}</p>
                <p>result : {resutl}</p>
            </div>
        );
    };
    render() {
        let { active } = this.state;
        return (
            <div>
                <div className='d-flex justify-content-between container'>
                    <button
                        onClick={() => {
                            this.handleChangeActive('TAI');
                        }}
                        className='btn btn-danger'
                        style={{
                            width: '150px',
                            height: '150px',
                            fontSize: '50px',
                            transform: `scale(${active == 'TAI' ? 1 : 0.5})`,
                        }}
                    >
                        Tài
                    </button>
                    <div>{this.renderListXucXac()}</div>
                    <button
                        onClick={() => {
                            this.handleChangeActive('XIU');
                        }}
                        className='btn btn-success'
                        style={{
                            width: '150px',
                            height: '150px',
                            fontSize: '50px',
                            transform: `scale(${active == 'XIU' ? 1 : 0.5})`,
                        }}
                    >
                        Xỉu
                    </button>
                </div>
                {this.renderResutl()}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    arrayXucSac: state.xucSacReducers.arrayXucSac,
    resutl: state.xucSacReducers.resutl,
    youChoose: state.xucSacReducers.youChoose,
    youWin: state.xucSacReducers.youWin,
    total: state.xucSacReducers.total,
});
const mapDispatchToProps = (dispatch) => {
    return {
        handleChangeActive: () => {
            dispatch({
                type: 'YOU_CHOOSE',
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(XucSac);
