import React, { Component } from 'react';
import { connect } from 'react-redux';

export class KetQua extends Component {
    render() {
        return (
            <div>
                <button
                    className='btn btn-secondary'
                    onClick={this.props.handlePlayGame}
                >
                    <h2>Play Game</h2>
                </button>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handlePlayGame: () => {
            dispatch({
                type: 'PLAY_GAME',
            });
        },
    };
};

export default connect(null, mapDispatchToProps)(KetQua);
