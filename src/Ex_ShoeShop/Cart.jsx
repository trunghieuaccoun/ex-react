import React, { Component } from 'react';

export default class Cart extends Component {
    renderTbody = () => {
        return this.props.cart.map((item, index) => {
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <img
                            style={{
                                width: '50px',
                            }}
                            src={item.image}
                            alt=''
                        />
                    </td>
                    <td>{item.price * item.number} $</td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.handdlePrductReduction(item);
                            }}
                            className='btn btn-dark'
                        >
                            -
                        </button>
                        <span className='mx-2'>{item.number}</span>
                        <button
                            className='btn btn-danger'
                            onClick={() => {
                                this.props.handleMoreProduct(item);
                            }}
                        >
                            +
                        </button>
                    </td>
                </tr>
            );
        });
    };
    render() {
        return (
            <div className='text-right'>
                <button
                    type='button'
                    className='btn btn-primary'
                    data-toggle='modal'
                    data-target='.bd-example-modal-lg'
                >
                    My Cart
                </button>
                <div
                    className='modal fade bd-example-modal-lg'
                    tabIndex={-1}
                    role='dialog'
                    aria-labelledby='myLargeModalLabel'
                    aria-hidden='true'
                >
                    <div className='modal-dialog modal-lg'>
                        <div className='modal-content'>
                            <table className='table'>
                                <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>Name</td>
                                        <td>Img</td>
                                        <td>Price</td>
                                        <td>Số Lượng</td>
                                    </tr>
                                </thead>
                                <tbody>{this.renderTbody()}</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
