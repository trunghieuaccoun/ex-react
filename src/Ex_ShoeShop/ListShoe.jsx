import React, { Component } from 'react';
import ItemShoe from './ItemShoe';

export default class ListShoe extends Component {
    renderListShoe = () => {
        return this.props.shoeArr.map((item, index) => {
            return (
                <ItemShoe
                    data={item}
                    key={index}
                    handleClick={this.props.handleChangeDetailShoe}
                    handleAddToCart={this.props.handleAddToCart}
                />
            );
        });
    };
    render() {
        return <div className='row'>{this.renderListShoe()}</div>;
    }
}
