import React, { Component } from 'react';

export default class DetailShoe extends Component {
    render() {
        let { id, name, alias, description, image, price, quantity } =
            this.props.detail;
        return (
            <div>
                {/* Button to Open the Modal */}
                {/* The Modal */}
                <div className='modal' id='myModal'>
                    <div className='modal-dialog'>
                        <div className='modal-content'>
                            {/* Modal Header */}
                            <div className='modal-header'>
                                <h4 className='modal-title text-primary'>
                                    Name :{name}
                                </h4>
                            </div>
                            {/* Modal body */}
                            <div className='modal-body'>
                                <div className='row mt-5 text-left alert-secondary'>
                                    <img
                                        src={image}
                                        alt={name}
                                        className='col-6'
                                    />
                                    <div className='col-6'>
                                        <p>ID :{id}</p>

                                        <p>Alias : {alias}</p>
                                        <p>price : {price}</p>
                                        <p>quantity : {quantity}</p>
                                        <p>Descripton : {description}</p>
                                    </div>
                                </div>
                            </div>
                            {/* Modal footer */}
                            <div className='modal-footer'>
                                <button
                                    type='button'
                                    className='btn btn-danger'
                                    data-dismiss='modal'
                                >
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
