import React, { Component } from 'react';
import { dataShoe } from './DataShoe';
import DetailShoe from './DetailShoe';
import Cart from './Cart';
import ListShoe from './ListShoe';
export default class Ex_ShoeShop extends Component {
    state = {
        shoeArr: dataShoe,
        detail: dataShoe[0],
        cart: [],
    };
    handleChangeDetailShoe = (shoe) => {
        this.setState({
            detail: shoe,
        });
    };
    handleAddToCart = (shoe) => {
        let cloneCart = [...this.state.cart];
        let index = this.state.cart.findIndex((item) => {
            return item.id == shoe.id;
        });
        if (index == -1) {
            let cartItem = { ...shoe, number: 1 };
            cloneCart.push(cartItem);
        } else {
            cloneCart[index].number++;
        }
        this.setState({ cart: cloneCart });
        alert(
            'thêm vào giỏ hàng thành công',
            'vui lòng thử lại',
            [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
            { cancelable: false }
        );
    };
    handleMoreProduct = (shoe) => {
        let cloneCart = [...this.state.cart];
        let index = cloneCart.findIndex((item) => {
            return item.id == shoe.id;
        });

        if (cloneCart[index].number < 10) {
            cloneCart[index].number++;
        } else if (cloneCart[index].number == 'mua ít thôi bạn ơi') {
            cloneCart[index].number = 10;
        } else cloneCart[index].number = 'mua ít thôi bạn ơi';
        this.setState({ cart: cloneCart });
    };

    handdlePrductReduction = (shoe) => {
        let cloneCart = [...this.state.cart];
        let index = cloneCart.findIndex((item) => {
            return item.id == shoe.id;
        });

        if (cloneCart[index].number > 1) {
            cloneCart[index].number--;
        } else if (cloneCart[index].number == 1) {
            let cloneCartSplice = cloneCart.splice(`${index}`, 1);
        }
        this.setState({ cart: cloneCart });
    };
    render() {
        return (
            <div className='container mx-auto'>
                <h1 className='text-success'>Shoe Shop</h1>
                <Cart
                    cart={this.state.cart}
                    handleMoreProduct={this.handleMoreProduct}
                    handdlePrductReduction={this.handdlePrductReduction}
                />
                <ListShoe
                    shoeArr={this.state.shoeArr}
                    handleChangeDetailShoe={this.handleChangeDetailShoe}
                    handleAddToCart={this.handleAddToCart}
                />
                <DetailShoe detail={this.state.detail} />
            </div>
        );
    }
}
