import React, { Component } from 'react';

export default class ItemShoe extends Component {
    render() {
        let { image, name, price } = this.props.data;
        return (
            <div className='col-3 p-2'>
                <div className='card text-left h-100'>
                    <img className='card-img-top' src={image} alt={name} />
                    <div className='card-body'>
                        <h6 className='card-title'>{name}</h6>
                        <h2 className='card-title'>{price} $</h2>
                        <div className='d-flex justify-content-between'>
                            <button
                                onClick={() => {
                                    this.props.handleClick(this.props.data);
                                }}
                                className='btn btn-success '
                                data-toggle='modal'
                                data-target='#myModal'
                            >
                                Xem chi tiết
                            </button>
                            <button
                                onClick={() => {
                                    this.props.handleAddToCart(this.props.data);
                                }}
                                className='btn btn-danger'
                            >
                                Chọn mua
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
