import React, { Component } from 'react';
import ItemGlass from './ItemGlass';
import { dataGlass } from './DataGlass';
import ImprotImg from './ImprotImg';
import styles from './myCss.css';
export default class Ex_Glass extends Component {
    state = {
        glassArr: dataGlass,
        dataRow: dataGlass[0],
    };
    renderListGlass = () => {
        return this.state.glassArr.map((item, index) => {
            return (
                <ItemGlass
                    data={item}
                    key={index}
                    handleClick={this.handleClick}
                />
            );
        });
    };
    handleClick = (glass) => {
        this.setState({
            dataRow: glass,
        });
    };
    render() {
        return (
            <div className='background-fixed'>
                <ImprotImg dataRow={this.state.dataRow} />
                <div>
                    <div className='d-flex p-1'>{this.renderListGlass()}</div>
                </div>
            </div>
        );
    }
}
