import React, { Component } from 'react';

export default class ItemGlass extends Component {
    render() {
        let { url } = this.props.data;
        return (
            <div className=' bg-dark w-100  h-50 m-3'>
                <img
                    onClick={() => {
                        this.props.handleClick(this.props.data);
                    }}
                    className='card-img-top'
                    src={url}
                    alt='Card image cap'
                />
            </div>
        );
    }
}
