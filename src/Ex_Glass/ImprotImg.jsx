import React, { Component } from 'react';
import modeljpg from './glassesImage/model.jpg';

export default class ImprotImg extends Component {
    cardPosition = () => {
        let { name, desc, url } = this.props.dataRow;
        return (
            <div className='card position-absolute' style={{ width: '12rem' }}>
                <img className='card-img-top' src={url} alt='Card image cap' />
                <div className='card-body bg-secondary'>
                    <p className='card-title'>Name :{name}</p>
                    <p className='card-text'>Desc :{desc}</p>
                </div>
            </div>
        );
    };
    showModel = () => {
        return (
            <div className='d-flex  justify-content-between width-60 mx-auto padding13'>
                <img
                    src={modeljpg}
                    alt='image model'
                    className='glassModel position-relative'
                />
                <img src={modeljpg} alt='image model' className='glassModel' />
            </div>
        );
    };
    render() {
        return (
            <div>
                {this.showModel()}
                <div>{this.cardPosition()}</div>
            </div>
        );
    }
}
